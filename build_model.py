import sys
import pickle
import string


class Pipeline:
    def __init__(self, steps):
        self.steps = steps

    def transform(self, input):
        _input = input
        for step in self.steps:
            _input = step(_input)
        return _input


def word_tokenize(line):
    return line.split(" ")


discard_short_words = lambda words: [w for w in words if len(w) > 2]
lower = lambda words: [w.lower() for w in words]

stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
def discard_stop_words(words, language="english"):
    return list(filter(lambda w: w not in stopwords, words))


def discard_punctuation(s):
    return "".join([i for i in s if i not in string.punctuation])


pipeline = Pipeline(
    [
        discard_punctuation,
        word_tokenize,
        discard_short_words,
        lower,
        discard_stop_words,
    ]
)


data = []
labels = []

with open(sys.argv[1]) as fd:
    for line in fd:
        datum, label = line.replace('"', "").split(',')
        data.append(datum)
        labels.append(label)

print((len(data), len(labels)))

from sklearn.model_selection import train_test_split


data_dev, data_test, labels_dev, labels_test = train_test_split(data, labels, train_size=0.8, random_state=1, shuffle=True)
data_train, data_dev_test, labels_train, labels_dev_test = train_test_split(data_dev, labels_dev, train_size=0.8, shuffle=True, random_state=1)

len(data), len(data_dev), len(data_train)

from sklearn.feature_extraction.text import TfidfVectorizer

vectorizer = TfidfVectorizer(min_df=2, ngram_range=(1, 2),  stop_words='english',  strip_accents='unicode',  norm='l2')

X_train = vectorizer.fit_transform(data_train)
X_test = vectorizer.transform(data_dev_test)


from sklearn import tree
from sklearn.metrics import classification_report


clf = tree.DecisionTreeClassifier().fit(X_train.toarray(), labels_train)
y_tree_predicted = clf.predict(X_test.toarray())

print(' \n Here is the classification report:')
print(classification_report(labels_dev_test, y_tree_predicted))


def predict(q):
    clean_q = [" ".join(pipeline.transform(q))]
    print(clean_q)
    q = vectorizer.transform(clean_q)
    print(q)
    return clf.predict(q)[0]

qs = [
    "flask is failing with route initialization",
    "how can i send a prop to another component"
]

for q in qs:
    print(f"{q}: {predict(q)}")

model = {
    "vectorizer": vectorizer,
    "clf": clf
}

with open('model.pickle', 'wb') as f:
    pickle.dump(model, f, pickle.HIGHEST_PROTOCOL)
