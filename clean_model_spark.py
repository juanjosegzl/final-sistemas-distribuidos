from pyspark import SparkConf, SparkContext
import json
import string
from io import StringIO
import csv

conf = SparkConf().setMaster("local").setAppName("CleanWordCloud")
sc = SparkContext(conf = conf)


class Pipeline:
    def __init__(self, steps):
        self.steps = steps

    def transform(self, input):
        tags = input["tags"]
        _input = input
        for step in self.steps:
            _input = step(_input)

        tag = "NONE"
        if "python" in tags:
            tag = "python"
        if "javascript" in tags:
            tag = "javascript"
        return _input, tag


def word_tokenize(line):
    return line.split(" ")


discard_short_words = lambda words: [w for w in words if len(w) > 4]
lower = lambda words: [w.lower() for w in words]

stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
def discard_stop_words(words, language="english"):
    return list(filter(lambda w: w not in stopwords, words))


def discard_punctuation(s):
    return "".join([i for i in s if i not in string.punctuation])


pick_question = lambda x: x['question']
join_words = lambda x: " ".join(x)

pipeline = Pipeline(
    [
	pick_question,
        discard_punctuation,
        word_tokenize,
        discard_short_words,
        lower,
        discard_stop_words,
        join_words
    ]
)

input = sc.textFile("resultados.txt")
questions = input.map(lambda x: json.loads(x))

questions = questions\
    .map(pipeline.transform)\
    .filter(lambda q: q[1] != "NONE")

def writeRecords(records):
    """Write out CSV lines"""
    output = StringIO()
    writer = csv.writer(output)
    for record in records:
        writer.writerow(record)
    return [output.getvalue()]


questions.mapPartitions(writeRecords).saveAsTextFile("model")
