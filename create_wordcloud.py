import wordcloud
import sys
import json

line = next(sys.stdin)

freq = json.loads(line)

wordCloud = wordcloud.WordCloud(
    width=1024,
    height=768,
    max_words=60,
    background_color='white'
  ).generate_from_frequencies(freq)

wordCloud.to_file("wordcloud.png")
