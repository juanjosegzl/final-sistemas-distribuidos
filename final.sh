set -x

# Install kafka
wget https://downloads.apache.org/kafka/2.7.0/kafka_2.13-2.7.0.tgz -O kafka.tar.gz
tar -xf kafka.tar.gz
cd kafka*

# Run zookeeper and kafka as detached processes
nohup bin/zookeeper-server-start.sh config/zookeeper.properties &
nohup bin/kafka-server-start.sh config/server.properties &

TOPIC=clean_sentence

# Create a topic
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic $TOPIC
bin/kafka-topics.sh --list --bootstrap-server localhost:9092
bin/kafka-topics.sh --describe --bootstrap-server localhost:9092 --topic $TOPIC

# See events
#bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic $TOPIC --from-beginning

cd -

python3 read_events.py &
sleep 5
python3 write_events.py

sleep 5

today=$(date +"%Y%m%d")

hdfs dfs -ls questions.txt
# run mrjob

python3 sanitize.py -r hadoop hdfs:///user/hadoop/questions.txt -q | python3 format_mrjob_output.py | python3 create_wordcloud.py

aws s3 cp --acl public-read wordcloud.png s3://iteso-sisdists1/${today}.png

# sparksql
cp -rf resultados.txt mysparksql.txt
sed -i 's/\], "tags"/, "tags"/g' mysparksql.txt
sed -i 's/"answers_count": \[/"answers_count": /g' mysparksql.txt
sed -i 's/ views", "vote/", "vote/g' mysparksql.txt
sed -i 's/ view", "vote/", "vote/g' mysparksql.txt
# Upload file
hdfs dfs -put -f mysparksql.txt


# Install stb package
mkdir -p spark-stats/src/main/scala/

# Copy scala script to src dir & sbt configurations
cp -rf stats.scala spark-stats/src/main/scala/
cp -rf simple.sbt spark-stats
cd spark-stats

# Install SBT
curl https://bintray.com/sbt/rpm/rpm | sudo tee /etc/yum.repos.d/bintray-sbt-rpm.repo
sudo yum clean all
sudo yum -y install sbt

# Compute package scala
sbt package

# Submit job
spark-submit --class stats --master local target/scala-2.11/simple-project_2.11-1.0.jar

# Copy stats file at s3
hdfs dfs -cp /home/hadoop/stats/part-00000  s3://iteso-sisdists1/${today}-stats.txt
cd ~

# Run Spark jobs to create word count and clean corpus for training

spark-submit --master local clean_model_spark.py
spark-submit --master local clean_word_spark.py
