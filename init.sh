set -x

aws s3 cp install_python_dependencies.py s3://iteso-sisdists1/install_python_dependencies.sh

# To use in Windows
#cluster_id=$(\
#             aws emr create-cluster \
#                 --name "ProyectoFinal" \
#                 --release-label emr-5.32.0 \
#                 --applications Name=HADOOP Name=SPARK Name=HIVE Name=PIG \
#                 --ec2-attributes KeyName=sarakeys,SubnetId=subnet-3339187e \
#                 --instance-type m4.large \
#                 --instance-count 3 \
#                 --bootstrap-actions Path="s3://iteso-sisdists1/install_python_dependencies.sh",Name='Install Python Modules' \
#                 --use-default-roles | grep -Po "ClusterId\": \"\Kj-\w+(?=\")")


cluster_id=$(\
  aws emr create-cluster \
    --name "ProyectoFinal" \
    --release-label emr-5.32.0 \
    --applications Name=HADOOP Name=SPARK Name=HIVE Name=PIG \
    --ec2-attributes KeyName=sarakeys,SubnetId=subnet-3339187e \
    --instance-type m4.large \
    --instance-count 3 \
    --bootstrap-actions Path="s3://iteso-sisdists1/install_python_dependencies.sh",Name='Install Python Modules' \
    --use-default-roles | grep -Po "ClusterId\": \"\Kj-\w+(?=\")")

# cluster_id=j-10L9RGM32BTD6
# Upload scripts and data
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src events.jsonl
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src write_events.py
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src read_events.py
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src clean_word_spark.py
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src clean_model_spark.py
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src final.sh
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src simple.sbt
aws emr put --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --src stats.scala
aws emr ssh --cluster-id $cluster_id --key-pair-file ~/sarakeys.pem --command "sh final.sh"

#aws emr terminate-clusters --cluster-ids $cluster_id
