import json

import boto3
import pickle
import io
import string


class Pipeline:
    def __init__(self, steps):
        self.steps = steps

    def transform(self, input):
        _input = input
        for step in self.steps:
            _input = step(_input)
        return _input


def word_tokenize(line):
    return line.split(" ")


discard_short_words = lambda words: [w for w in words if len(w) > 2]
lower = lambda words: [w.lower() for w in words]

stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
def discard_stop_words(words, language="english"):
    return list(filter(lambda w: w not in stopwords, words))


def discard_punctuation(s):
    return "".join([i for i in s if i not in string.punctuation])


pipeline = Pipeline(
    [
        discard_punctuation,
        word_tokenize,
        discard_short_words,
        lower,
        discard_stop_words,
    ]
)

BUCKET_NAME = 'iteso-sisdists'
    
def load_pickle_from_s3(filename):
    s3 = boto3.resource('s3')
    pickle_object = s3.Object(BUCKET_NAME, filename)
    
    pickle_s = pickle_object.get()['Body'].read()
    pickle_fd = io.BytesIO(pickle_s)
    
    return pickle.load(pickle_fd)


model = load_pickle_from_s3('model.pickle')
vectorizer = model["vectorizer"]
clf = model["clf"]

def predict(q):
    clean_q = [" ".join(pipeline.transform(q))]
    q = vectorizer.transform(clean_q)
    return clf.predict(q)[0]
    

def lambda_handler(event, context):
    q = event["queryStringParameters"]["q"]
    tag = predict(q).strip()
    return {
        'headers': {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin': 'https://master.dm4r96voioss1.amplifyapp.com',
            'Access-Control-Allow-Methods': 'GET',
            'Content-Type': 'application/json',
        },
        'statusCode': 200,
        'body': json.dumps({"language": tag})
    }
