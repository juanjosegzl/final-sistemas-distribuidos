from kafka import KafkaConsumer
import time

KAFKA_OUTPUT_TOPIC_NAME_CONS = "clean_sentence"
KAFKA_BOOTSTRAP_SERVERS_CONS = "localhost:9092"

import subprocess


def run_cmd(args_list):
    print("Running system command: {0}".format(" ".join(args_list)))
    proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    s_output, s_err = proc.communicate()
    s_return = proc.returncode
    return s_return, s_output, s_err


if __name__ == "__main__":
    print("Kafka consumre started")
    consumer = KafkaConsumer(
        KAFKA_OUTPUT_TOPIC_NAME_CONS,
        bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS_CONS,
        auto_offset_reset="latest",
        enable_auto_commit=True,
    )

    print("Reading messages")

    for message in consumer:
        print("Key: ", message.key)
        output_message = message.value
        print("Message received", output_message)
        if output_message == b"THEEND":
            break

        with open("resultados.txt", "ab+") as fd:
            fd.write(output_message + bytes("\n", "utf-8"))

    run_cmd(["hdfs", "dfs", "-put", "-f", "resultados.txt", "questions.txt"])
