"""
    stackoverflow_crawling.py

    This script does crawling of stack overflow

"""

import json
import jsonlines
import re
import requests
from bs4 import BeautifulSoup
from datetime import datetime
from time import sleep
from json import dumps
from kafka import KafkaProducer


producer = KafkaProducer(bootstrap_servers=['den00hos.us.oracle.com:9092'],
value_serializer=lambda x:
    dumps(x).encode('utf-8'))


def get_time_now():
    now = datetime.now()
    return now.strftime("%m%d%Y")


ADDING = 8
COUNT_JSON = 7000
GRAL_COUNT =  COUNT_JSON * ADDING

for idx in range(1,GRAL_COUNT,ADDING):
    my_request = f"https://stackoverflow.com/questions?tab=newest&page={idx}"
    res = requests.get(my_request)
    print(f'Executing request: {my_request}')
    soup = BeautifulSoup(res.text, "html.parser")
    questions_data = {
        "questions": []
    }
    # 50 questions
    questions = soup.select(".question-summary")
    
    for que in questions:
        q = que.select_one('.question-hyperlink').getText()
        vote_count = que.select_one('.vote-count-post').getText()
        views = que.select_one('.views').attrs['title']
        try:
            dummy_date=que.find("span",class_='relativetime').attrs['title']
        except:
            pass
            # print("Date not found, taking as previous date")
        
        # Get date in format YYYYMMDD
        datep = re.findall(r'\d{4}-\d{2}-\d{2}', dummy_date)[0]
        datep = datep.replace('-', '')
        answers_count = re.findall(
            r'\d+', que.select_one('div.stats > div.status').getText()
        )
        tags = [i.getText() for i in (que.select('.post-tag'))]
        questions_data['questions'].append({
            "question": q,
            "views": views,
            "vote_count": vote_count,
            "answers_count": answers_count,
            "tags": tags,
            "date": datep
        })
    
    json_questions=questions_data['questions']

    with jsonlines.open(f'{datep}.jsonl', 'a') as writer:
        writer.write_all(json_questions)
    producer.send('streaming_data',value=json_questions)
    sleep(1)
    print(json_questions)