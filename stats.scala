import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
//import org.apache.spark.implicits._
import org.apache.spark.sql.functions._


object stats{
  def main(args : Array[String]): Unit = {
    var conf = new SparkConf().setAppName("Read CSV File").setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    import sqlContext.implicits._
    val df = sqlContext.jsonFile("/user/hadoop/mysparksql.txt")
    
    df.select(min("vote_count"),max("vote_count"), mean("vote_count"), min("answers_count"),max("answers_count"), mean("answers_count"), min("views"),max("views"), mean("views")).show()
 
    df.select(min("vote_count"),max("vote_count"), mean("vote_count"), min("answers_count"),max("answers_count"), mean("answers_count"), min("views"),max("views"), mean("views")).rdd.saveAsTextFile("/home/hadoop/stats")

    //df.agg(min("vote_count"),max("vote_count"), mean("vote_count"), min("answers_count"),max("answers_count"), mean("answers_count"), min("views"),max("views"), mean("views")).head()

    //df.agg(min("vote_count"),max("vote_count"), mean("vote_count"), min("answers_count"),max("answers_count"), mean("answers_count"), min("views"),max("views"), mean("views")).rdd.saveAsTextFile("/home/hadoop/stats")

  }
}

