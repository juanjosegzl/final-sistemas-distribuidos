from kafka import KafkaProducer
from datetime import datetime
import time
from json import dumps
import random
import csv

KAFKA_TOPIC_NAME_PROD = 'clean_sentence'
KAFKA_BOOTSTRAP_SERVERS_PROD = 'localhost:9092'

if __name__ == '__main__':
    print('kafka producer started')

    kafka_producer_obj = KafkaProducer(
        bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS_PROD
    )

    message = None

    with open('events.jsonl', 'r') as fd:
        reader = csv.reader(fd)
        for message in reader:
            message = ",".join(message)
            kafka_producer_obj.send(
                KAFKA_TOPIC_NAME_PROD,
                message.encode('utf-8')
            )
            # this one is important
            time.sleep(0.1)
        kafka_producer_obj.send(
            KAFKA_TOPIC_NAME_PROD,
            "THEEND".encode('utf-8')
        )
        time.sleep(0.1)
